import { Component, OnInit, Input } from '@angular/core';
import { Song } from '../entity/song';

@Component({
  selector: 'app-footer-player',
  templateUrl: './footer-player.component.html',
  styleUrls: ['./footer-player.component.css']
})
export class FooterPlayerComponent implements OnInit {

  @Input() song:Song;

  constructor() { }

  ngOnInit() {
  }

}
